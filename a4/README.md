
# LIS4381

## Reese White

### Assignment 4 Requirements:

*Requirements:*

1. Create Web App to display lis4381 work
2. Pet Store entry fields match Pet Store Database
3. Entries must show failed and passed validations
4. Skillsets 10-12

#### README.md file should include the following items:

* Screenshots of Failed Validation
* Screenshot of Passed Validation
* Link to LIS4381 Assignment Portal
* Screenshots of skillsets 10-12

> #### LIS4381 Web App link:
*LIS4381 Portal:*
[LIS4381 Web App](http://localhost/repos/lis4381/index.php# "http://localhost/repos/lis4381/")

#### Assignment Screenshots:

*LIS4381 Portal (Main Page):*

![LIS4381 Portal Main Page](img/a4a.png)

*Failed Validation:*

![Failed Validation](img/a4b.png)

*Passed Validation:*

![Passed Validation](img/a4c.png)

#### Skillset Screenshots:

|Skill Set 10: Array List|Skill Set 11: Alpha Numeric Special|Skill Set 12: Temperature Conversion|
|:-:|:-:|:-:|
|![Skill Set 4](img/ss10.png)|![SKill Set 5](img/ss11.png)|![Skill Set 6](img/ss12.png)|
