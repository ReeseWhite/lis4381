# LIS4381 

## Reese White

### Assignment 5 Requirements:

*Three Parts:*

1. Create Basic Sever/Client Side Validation for database entries
2. Connect Database to allow entries to update once they have passed validation. 
3. Complete Java and PHP Skillsets 

#### README.md file should include the following items:

* Screenshot of index.php
* Screenshot of invalid client side entry
* Screenshot of invalid server side entry
* Screenshot of valid client/server side entry
* Screenshot of entry passing full validation and being added to database
* Screenshots of Java and PHP skillsets

> 
>
>
>
> 

#### Assignment Screenshots:

*index.php:*

![index.php](img/a5a.png)

*Invalid Client Side:*
![invalid](img/a5b.png)
*Invalid Server Side:*
![failed validation](img/a5c.png)|

*Valid Client Side:*
![valid entry](img/a5d.png)
*Passed Validation:*
![passed validation](img/a5e.png)|

#### LIS4381 Web App link:
*LIS4381 A5:*
[LIS4381 Web App](http://localhost/repos/lis4381/a5/index.php "http://localhost/repos/lis4381/a5/index.php")

#### Skillset Screenshots:

#### Sphere Volume Calculator

![Skillset 13](img/s13a.png)

#### Simple Calculator

![index](img/s14a.png)

![addition](img/s14b.png)

![index2](img/s14c.png)

![division](img/s14d.png)

#### Write/Read File

![index.php](img/s15a.png)

![process.php](img/s15b.png)