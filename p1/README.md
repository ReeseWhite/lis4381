> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 

## Reese White

### Project 1 Requirements:

*Three Parts:*

1. Reverse Engineer Mobile Application
2. Skillsets 7-9
3. Chapter Questions (Chs 7, 8)

#### README.md file should include the following items:

* Screenshots of first Mobile App UI 
* Screenshots of second Mobile App UI 
* Screenshots of skillsets

> 
>
>
>
> 

#### Mobile App Screenshots:

|First UI|Second UI|
|:-:|:-:|
|![First UI](img/p1a.png)|![Second UI](img/p1b.png)|

#### Skillset Screenshots:

|Skill Set 7: Random Array|Skill Set 8: Largest of Three Numbers|Skill Set 9: Array Runitme|
|:-:|:-:|:-:|
|![Skill Set 7](img/p1c.png)|![SKill Set 8](img/p1d.png)|![Skill Set 9](img/p1e.png)|

