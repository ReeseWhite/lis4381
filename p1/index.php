<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Reese White">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Project 1</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong>In this project we were asked to reverse engineer a Mobile App provided by our Professor. The app is made to function as a business card to represent ourselves. An image of ourselves with a button that leads to contact information and our interests. The final part is to include our Java skillsets.
				</p>

				<h4>First UI</h4>
				<img src="img/p1a.png" class="img-responsive center-block" alt="First UI">

				<h4>Details Screen</h4>
				<img src="img/p1b.png" class="img-responsive center-block" alt="Details Screen">

				<h4>Skillset 7</h4>
				<img src="img/p1c.png" class="img-responsive center-block" alt="Skillset 7">

				<h4>Skillset 8</h4>
				<img src="img/p1d.png" class="img-responsive center-block" alt="Skillset 8">

				<h4>Skillset 9</h4>
				<img src="img/p1e.png" class="img-responsive center-block" alt="Skillset 9">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
