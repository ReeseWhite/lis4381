> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Reese White

### Assignment 3 Requirements:

*Three Parts:*

1. Database with 3 tables and 10 unique entries
2. Create Concert Mobile App
3. Chapter Questions (Chs 5,6)

#### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of running application first user interface
* Screenshot of running application second user interface
* Screenshots of 10 records for each table

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshots of First and Second User Interface*:*

|First UI|Second UI|
|:-:|:-:|
|![First UI](img/a3a.png)|![Second UI](img/a3b.png)|

*Screenshot of ERD:*

![ERD](img/erd.png)

*Screenshots of Table Entries:*

![Pet Store Table](img/a3c.png)|![Pet Table](img/a3d.png)|![Customer Table](img/a3e.png)|


#### mySQL Links:

*mySQL link:*
[mySQL](docs/a3.sql)

*mySQL MWB link:*
[MWB](docs/a3.mwb)


*Screenshots of Java SKill Sets*:

|Skill Set 4: Decision Structures|Skill Set 5: Nested Structures|Skill Set 6: Methods|
|:-:|:-:|:-:|
|![Skill Set 4](img/ss4.png)|![SKill Set 5](img/ss5.png)|![Skill Set 6](img/ss6.png)|
