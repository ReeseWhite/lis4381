<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Reese White">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment 2</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong>In this assignment we created a mobile app using Android Stuido to display a recipe. We needed to include the provided image and create a functioning button to bring the user to an ingredient page. After gettign the app to function we were required to change some of the colors and make an overall theme.
				</p>

				<h4>Splash Screen</h4>
				<img src="img/a2a.png" class="img-responsive center-block" alt="Splash Screen">

				<h4>Ingredient Screen</h4>
				<img src="img/a2b.png" class="img-responsive center-block" alt="Ingredient Screen">

				<h4>Skillset 1</h4>
				<img src="img/ss1.png" class="img-responsive center-block" alt="Skillset 1">

				<h4>Skillset 2</h4>
				<img src="img/ss2.png" class="img-responsive center-block" alt="Skillset 2">

				<h4>Skillset 3</h4>
				<img src="img/ss3.png" class="img-responsive center-block" alt="Skillset 3">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
