> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Reese White

### Assignment 2 Requirements:

*Three Parts:*

1. Create Healthy Recipes Android app
2. Chapter Questions (Chs 3,4)
3. Bitbucket repo link

#### README.md file should include the following items:

* Screenshots of running mobile app
* Screenshots of Java skill sets


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of Healhty Recipes Mobile App*:

|Splash Screen|Ingredients Screen|
|:-:|:-:|
|![Splash Screen](img/a2a.png)|![Ingredients Screen](img/a2b.png)|

*Screenshots of Java SKill Sets*:

|Skill Set 1: Even Or Odd|Skill Set 2: Largest of Two Integeres|Skill Set 3: Arrays and Loops|
|:-:|:-:|:-:|
|![Skill Set 1](img/ss1.png)|![SKill Set 2](img/ss2.png)|![Skill Set 3](img/ss3.png)|
