> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**

# LIS4381 - Mobile Web App Development

## Reese White

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Install JDK
    - Install Android Studio and create My First App
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials (bitbucketstationlocations)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file") 
    - Create Healhty Recipes Android app
    - Provide screenshots of completed app
    - Provide screenshots of completed Java skill sets

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create MyEvent Mobile Application
    - Create ERD for Petstore data
    - Provide Screenshots of completed Java skill sets
    - Provide Screenshots of table entries

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Create Web App to display lis4381 work
    - Create entries to represent Pet Store database
    - Make sure each entry has proper validation
    - Provide Screenshots of passed and failed validation
    - Screenshots of Java skillsets

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Connect Database to PHP index
    - Create Add function to database
    - Create and Test Client Side Validation
    - Create and Test Server Side Validation

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Reverse Engineer Business Card Mobile App
    - Provide screenshots of first and second UI
    - Provide screenshots of completed Java skill sets

7. [P2 README.md](p2/README.md "My P2 README.md file")  
    - Add Delete Record Function
    - Add Edit Record Function
    - Create RSS Feed 
    - Complete Validation for Entries
    - Successfully edit an entry
    - Successfully delete an entry