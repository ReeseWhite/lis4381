> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 

## Reese White

### Project 2 Requirements:

*Three Parts:*

1. Add Delete function to database page
2. Add an Edit function to database page
3. Add RSS Feed 

#### README.md file should include the following items:

* Screenshot of Home Page
* Screenshot of P2 index.php
* Screenshot of edit_petstore.php
* Screenshot of failed validation
* Screenshot of passed validation
* Screenshot of delete record prompt
* Screenshot of successfully deleted record
* Screenshot of RSS Feed
>
>
>
>
>

#### Assignment Screenshots:

*Screenshot of Home Page*:

![Home Page](img/p2a.png)

*Screenshot of index.php*:

![index](img/p2b.png)

*Screenshot of edit_petstore.php*:

![Edit Petstore](img/p2c.png)

*Screenshot of Falied Validation*:

![Failed Validation](img/p2d.png)

*Screenshot of Passed Validation*:

![Passed Validation](img/p2e.png)

*Screenshot of Delete Record Prompt*:

![Delete Record Prompt](img/p2f.png)

*Screenshot of Successfully Deleted Record*:

![Deleted Record](img/p2g.png)

*Screenshot of RSS Feed*:

![RSS Feed](img/p2h.png)


#### Local LIS4381 web app:

*My Local Web App:*
[Web App](http://localhost/repos/lis4381/ "Local Web App")

